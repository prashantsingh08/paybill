var getlooser = new getLooser;

function getLooser() {
    this.applicants = [];

    this.init = function() {
        this.addApplicants();
        this.getRandomUser();
        this.runAgain();
        this.startOver();
    }

    this.showList = function() {
        var parent = document.querySelector('.applicant-list-wrapper');
        var template = '';
        for(var i = 0; i < this.applicants.length; i++) {
            template += '<span class="name-tag" data-id="'+ i +'">'+ this.applicants[i] +'</span>';
        }
        parent.innerHTML = '';
        parent.insertAdjacentHTML('afterbegin', template);
        this.deleteOne();
    }

    this.addApplicants = function() {
        var $this = this;

        function generateList(input) {
            var value = input.value;
            if($this.checkValid(value.toLowerCase())) {
                $this.applicants.push(value.toLowerCase());
                input.value = '';

                $this.showList();
            } else {
                alert(value + ' or ' + value.toLowerCase() +' already in list.')
            }
        }
        
        var addBtn = document.querySelector('#add-applicant');
        addBtn.addEventListener('click', function() {
            var input = document.querySelector('#applicant-value');

            generateList(input)
        });
    }

    this.checkValid = function(value) {
        if(this.applicants.indexOf(value) < 0 && value != '') {
            return true;
        } 
        return false;
    }

    this.getRandomUser = function() {
        var $this = this;
        var resultsButton = document.querySelector('#show-results');

        function showLooser() {
            var resultsContainer = document.querySelector('.results-container');
            var applicantsContainer = document.querySelector('.applicant-container');
            applicantsContainer.className += ' hidden';
            resultsContainer.className = 'results-container';
            $this.showRandomUser();
        }
        resultsButton.addEventListener('click', function(event) {
            if($this.applicants.length > 1) {
                showLooser();
            } else {
                alert('You need more users!');
            }
        });
    }

    this.showRandomUser = function() {
        var resultsContainer = document.querySelector('.result');
        var rand = this.applicants[Math.floor(Math.random() * this.applicants.length)];
        resultsContainer.innerHTML = '';
        resultsContainer.insertAdjacentHTML('afterbegin', '<h3>'+ rand +'</h3>');
    }

    this.runAgain = function() {
        var $this = this;
        var runAgainBtn = document.querySelector('.run-again');
        runAgainBtn.addEventListener('click', function(event) {
            $this.showRandomUser();
        });
    }

    this.startOver = function() {
        var $this = this;
        var startOverBtn = document.querySelector('.start-again');

        startOverBtn.addEventListener('click', function(event) {
            var resultsContainer = document.querySelector('.results-container');
            var applicantsContainer = document.querySelector('.applicant-container');
            var applicantsWrapper = document.querySelector('.applicant-list-wrapper');
            resultsContainer.className = 'results-container hidden';
            applicantsContainer.className = 'applicant-container';
            applicantsWrapper.innerHTML = '';
            $this.applicants = [];
        });
    }

    this.deleteOne = function() {
        var $this = this;
        var item = document.querySelectorAll('.name-tag');

        function removeIt(element) {
            var attr = parseInt(element.getAttribute('data-id'));
            $this.applicants.splice(attr, 1);
            $this.showList();
        }
        for(var i = 0; i < item.length; i++) {
            item[i].addEventListener('click', function(event) {
                removeIt(this);
            })
        }
    }

}
getlooser.init();